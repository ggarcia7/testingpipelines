LM_API_AUTH_BASEURL='http://appsuat.lifemiles.net/integrator/v1'
LM_API_BASEURL='http://appsuat.lifemiles.net/lifemiles'
LM_BUMP_VERSION_TYPE=''
LM_AWS_STORAGE_FOLDER='uat/'
RANCHER_ENV_ID='1a16'
LM_RANCHER_SERVICE_CHOICE = 'Staging/mobile-api'
LM_PUSHWOOSH_APPLICATION_ID = 'E3A4A-2ECC5'

LM_DB_CREDENTIALS='mysql_container'
LM_AWS_CREDENTIALS='S3LifemilesCredentials'
LM_NEXUS_CREDENTIALS='applaudo_nexus'
LM_PUSHWOOSH_CREDENTIALS='PushwooshToken'
LM_RSA_CREDENTIAL='RSA_KEY_UAT'

LM_S3_ENDPOINT_URL = 'https://applaudo-cms-lifemiles-app-uat.s3.amazonaws.com'
LM_BUCKET_NAME = 'applaudo-cms-lifemiles-app-uat'
LM_AWS_REGION = 'eu-west-1'
LM_MYSQL_HOST = 'database'
MYSQL_DATABASE = 'lifemiles'
LM_MYSQL_ROOT_PASSWORD = 'rootpassword'
PUSHWOOSH_BASEURL = 'https://cp.pushwoosh.com/json/1.3'
DOCKER_REGISTRY_URL = "nexus-prov-snapshot.lifemiles.net"
PROJECT_NAME = "mobile-api"
